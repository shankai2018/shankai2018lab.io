import os
import gitlab
import pprint
import pickle
from datetime import date
from chart import bar
"""
通过 python-gitlab 模块, 计算每个人 is/ic/ci/cc 个数, 并取出 top5 图形化显示
"""
_url = 'https://gitlab.com'
_projectid = 16345223
_token = os.environ['GITLAB_TOKEN']
print('gitlab 紧急连接中...')
try:
    with gitlab.Gitlab(_url,private_token=_token,timeout=30,per_page=100) as _gl:
        _project =_gl.projects.get(_projectid, lazy=True,retry_transient_errors=True,timeout=50)
except Exception as e:
    raise e
_events_notes = _project.events.list(action='commented',as_list=False,lazy=True,retry_transient_errors=True,timeout=50)
_events_issues = _project.events.list(action='created',target_type='issue',as_list=False,lazy=True,retry_transient_errors=True,timeout=50)
_events_commits = _project.events.list(action='pushed',as_list=False,lazy=True,retry_transient_errors=True,timeout=50)
_termgraph_dict={}
print('数据统计中, 再给我 2 分钟~')
for _item in _events_notes:
    _note = _item.attributes
    if _note['note']['noteable_type'] == 'Issue':
        if _note['author']['username'] in _termgraph_dict:
            _termgraph_dict[_note['author']['username']]['issue_comment'] += 1
        else:
            _termgraph_dict[_note['author']['username']] = {'issue_create':0,'issue_comment':1,'commit_create':0,'commit_comment':0}
    elif _note['note']['noteable_type'] =="Commit":
        if _note['author']['username'] in _termgraph_dict:
            _termgraph_dict[_note['author']['username']]['commit_comment'] += 1
        else:   
            _termgraph_dict[_note['author']['username']] = {'issue_create':0,'issue_comment':0,'commit_create':0,'commit_comment':1}
    else:
        pass

for _item in _events_issues:
    _issues = _item.attributes
    if _issues['author']['username'] in _termgraph_dict:
        _termgraph_dict[_issues['author']['username']]['issue_create'] += 1
    else:
        _termgraph_dict[_issues['author']['username']] = {'issue_create':1,'issue_comment':0,'commit_create':0,'commit_comment':0}

for _item in _events_commits:
    _commits = _item.attributes
    if _commits['author']['username'] in _termgraph_dict:
        _termgraph_dict[_commits['author']['username']]['commit_create'] += _commits['push_data']['commit_count']
    else:
        _termgraph_dict[_commits['author']['username']] = {'issue_create':0,'issue_comment':0,
                                                        'commit_create':_commits['push_data']['commit_count'],'commit_comment':0}
# pprint.pprint(_termgraph_dict)
# with open('termgraph_dict.pickle','wb') as f:
#     pickle.dump(_termgraph_dict,f)
def _graph(top5,which):
    _top5 = sorted(_termgraph_dict.items(),key=lambda _dict:_dict[1][which], reverse=True)[0:5]
    x,y=[],[]
    for (k,v) in _top5:
        x.append(v[which])
        y.append(k+' '+str(v[which]))
    print('##',top5,'\n')
    # print("```")
    bar(x, y)
    # print("```\n")
    print("\n")

print(f'{date.today().__str__()}:')
_top5_list=[('is_top5:','issue_create'),('ic_top5:','issue_comment'),('ci_top5:','commit_create'),('cc_top5:','commit_comment')]
for _top5 in _top5_list:
    _graph(_top5[0],_top5[1])


